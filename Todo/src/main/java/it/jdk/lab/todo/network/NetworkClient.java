package it.jdk.lab.todo.network;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import it.jdk.lab.todo.Contract;

/**
 * Created by Flavio on 15/11/13.
 */
public class NetworkClient extends IntentService {

    private static final String REPLY_TO = "replay_to";
    private static final String METHOD = "method";
    private static final int DEFAULT = -1;
    private static final int GET = 0;
    private static final int POST = 1;
    private static final int PUT = 2;
    private static final int PATCH = 3;
    private static final int DELETE = 4;


    // Methods for hiding informations to the activity
    public static void download(Context context, String url, String action) {
        context.startService(new Intent(context, NetworkClient.class)
                .setData(Uri.parse(url))
                .putExtra(REPLY_TO, action)
                .putExtra(METHOD, GET));

    }

    public static void upload(Context context, String url, String ids) {

        context.startService(new Intent(context, NetworkClient.class)
                .setData(Uri.parse(url))
                .putExtra("IDS", ids)
                .putExtra(METHOD, POST));
    }

    public static void upload(Context context, String url, Long id) {
        context.startService(new Intent(context, NetworkClient.class)
                .setData(Uri.parse(url))
                .putExtra("ID", id)
                .putExtra(METHOD, POST));
    }

    public static void delete(Context context, String url, String ids) {
        context.startService(new Intent(context, NetworkClient.class)
                .setData(Uri.parse(url))
                .putExtra("IDS", ids)
                .putExtra(METHOD, DELETE));
    }

    public NetworkClient(){
        super("Network Client");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String token = getToken();
        if(token != null){
            Uri uri = intent.getData();
            HttpURLConnection connection = null;
            InputStream inputStream = null;

            // Work on the connection
            try {
                URL url = new URL(uri.toString());
                switch (intent.getIntExtra(METHOD, DEFAULT)){
                    case GET:
                        connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("GET");
                        connection.setRequestProperty("Accept","*/*");
                        connection.connect();

                        // Checking and processing server response
                        if(connection.getResponseCode()/100 == 2){
                            inputStream = connection.getInputStream();
                            Scanner scanner = new Scanner(new InputStreamReader(inputStream,  "UTF-8"));
                            String result = "";
                            while(scanner.hasNextLine()){
                                result += scanner.nextLine() + "\n";
                            }

                            JSONObject jsonObj = new JSONObject(result);
                            JSONArray jsonarray = jsonObj.getJSONArray("result");
                            Log.d("Service.Net", jsonObj.toString());

                            for(int position=0; position<jsonarray.length(); position++) {
                                //Create the ContentValue object in method saveResult
                                JSONObject arrayItem = jsonarray.getJSONObject(position);
                                saveResult(arrayItem);
                            }
                        }
                        break;
                    case POST:
                        DefaultHttpClient httpclient = new DefaultHttpClient();
                        HttpPost httpost = new HttpPost(url.toString());
                        JSONObject holderPost;

                        // Send JSON Array
                        if(intent.getStringExtra("IDS") != null){
                            holderPost = getJsonArrayById(intent.getStringExtra("IDS"));
                            StringEntity se = new StringEntity(holderPost.toString());
                            httpost.setEntity(se);


                            httpost.setHeader("Content-type", "application/json");
                            httpost.setHeader("Accept", "application/json");

                            // Execute Http POST request
                            HttpResponse res = httpclient.execute(httpost);
                            //Log.d("STATUS_CODE", String.valueOf(res.getStatusLine().getStatusCode()));
                            if(res.getStatusLine().getStatusCode()/100 == 2){
                                ContentValues values = new ContentValues();
                                values.put(Contract.KEY_SYNC, 2);
                                ContentResolver cr = getContentResolver();
                                cr.update(Uri.parse(Contract.CONTENT_URI + "/" + intent.getStringExtra("IDS")), values, null, null);
                            }
                        }
                        // Send JSON Object
                        else{
                            holderPost = getJsonObjectById(intent.getLongExtra("ID", 0));
                            holderPost.put("token", token);

                            StringEntity se = new StringEntity(holderPost.toString());
                            httpost.setEntity(se);

                            httpost.setHeader("Content-type", "application/json");
                            httpost.setHeader("Accept", "application/json");

                            // Execute Http POST request
                            HttpResponse res = httpclient.execute(httpost);
                            //Log.d("STATUS_CODE", String.valueOf(res.getStatusLine().getStatusCode()));
                            if(res.getStatusLine().getStatusCode()/100 == 2){
                                ContentValues values = new ContentValues();
                                values.put(Contract.KEY_SYNC, 2);

                                try{
                                    String result = EntityUtils.toString(res.getEntity());
                                    JSONObject j = new JSONObject(result);
                                    String uuid = j.getString("_id");

                                    values.put(Contract.KEY_UUID, uuid);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                ContentResolver cr = getContentResolver();
                                cr.update(Uri.parse(Contract.CONTENT_URI + "/" + intent.getLongExtra("ID", -1)), values, null, null);
                            }
                        }
                        break;

                    case PUT:
                        break;

                    case DELETE:
                        connection = (HttpURLConnection) url.openConnection();
                        connection.setRequestMethod("DELETE");
                        connection.setRequestProperty("Content-Type", "application/json");
                        JSONObject holderDelete;

                        // We put Jsonobject in the request body
                        if(intent.getStringExtra("IDS") != null){
                            holderDelete = getUUIdsJsonById(intent.getStringExtra("IDS"));
                            connection.setRequestProperty("Json", holderDelete.toString());
                            connection.connect();
                            if (connection.getResponseCode() / 100 == 2) {
                                ContentResolver cr = getContentResolver();
                                cr.delete(Uri.parse(Contract.CONTENT_URI + "/" + intent.getStringExtra("IDS")), null, null);
                            }
                        }
                        break;

                    default:
                        break;
                }

            } catch (MalformedURLException e) {

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }  finally {   // Close inputStream and Connection
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (connection != null) {
                    connection.disconnect();
                }
            }
        }
    }

    /**
     * Method to create JSON object from selected Item
     * @param id
     * @return
     * @throws org.json.JSONException
     */
    private JSONObject getJsonObjectById(Long id) throws JSONException {

        ContentResolver cr = getContentResolver();
        Cursor curs=cr.query(Uri.parse(Contract.CONTENT_URI + "/" + id), null, null, null, null);
        curs.moveToFirst();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(Contract.KEY_NAME, curs.getString(curs.getColumnIndex(Contract.KEY_NAME)));
        jsonObject.put(Contract.KEY_DATE, curs.getString(curs.getColumnIndex(Contract.KEY_DATE)));
        jsonObject.put(Contract.KEY_CHECK, curs.getInt(curs.getColumnIndex(Contract.KEY_CHECK)));
        if(!curs.getString(curs.getColumnIndex(Contract.KEY_UUID)).equals("")){
            jsonObject.put("_id", curs.getString(curs.getColumnIndex(Contract.KEY_UUID)));
        }

        ContentValues values = new ContentValues();
        values.put(Contract.KEY_SYNC, 1);
        cr.update(Uri.parse(Contract.CONTENT_URI + "/" + id), values, null, null);

        return jsonObject;
    }

    /**
     * Method to create JsonArray from selected items
     * @param ids
     * @return
     * @throws JSONException
     */
    private JSONObject getJsonArrayById(String ids) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        ContentResolver cr = getContentResolver();

        Cursor curs=cr.query(Uri.parse(Contract.CONTENT_URI+"/"+ids), null, null, null, null);

        if(curs != null){
            curs.moveToFirst();
            do{
                JSONObject json = new JSONObject();
                json.put(Contract.KEY_NAME, curs.getString(curs.getColumnIndex(Contract.KEY_NAME)));
                json.put(Contract.KEY_DATE, curs.getString(curs.getColumnIndex(Contract.KEY_DATE)));
                json.put(Contract.KEY_CHECK, curs.getInt(curs.getColumnIndex(Contract.KEY_CHECK)));
                if(!curs.getString(curs.getColumnIndex(Contract.KEY_UUID)).equals("")){
                    jsonObject.put("_id", curs.getString(curs.getColumnIndex(Contract.KEY_UUID)));
                }
                jsonArray.put(json);
            }while(curs.moveToNext());
        }
        jsonObject.put("JsonArray", jsonArray);

        // Update SYNC value
        ContentValues values = new ContentValues();
        values.put(Contract.KEY_SYNC, 1);
        cr.update(Uri.parse(Contract.CONTENT_URI + "/" + ids), values, null, null);

        return jsonObject;
    }

    /**
     * Method to create JsonArray from selected items
     * @param ids
     * @return
     * @throws JSONException
     */
    private JSONObject getUUIdsJsonById(String ids) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        ContentResolver cr = getContentResolver();

        Cursor curs=cr.query(Uri.parse(Contract.CONTENT_URI+"/"+ids), null, null, null, null);
        if(curs != null){
            curs.moveToFirst();
            do{
                JSONObject json = new JSONObject();
                if(!curs.getString(curs.getColumnIndex(Contract.KEY_UUID)).equals("")){
                    jsonObject.put("_id", curs.getString(curs.getColumnIndex(Contract.KEY_UUID)));
                }
                jsonArray.put(json);
            }while(curs.moveToNext());
        }
        jsonObject.put("JsonArray", jsonArray);
        // Update SYNC value
        ContentValues values = new ContentValues();
        values.put(Contract.KEY_SYNC, 1);
        cr.update(Uri.parse(Contract.CONTENT_URI + "/" + ids), values, null, null);

        return jsonObject;
    }

    /**
     * This method saves the todos on the database
     * @param jsonObject item attributes
     */
    private void saveResult(JSONObject jsonObject){
        ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();
        try {
            values.put(Contract.KEY_NAME, jsonObject.getString("name"));
            values.put(Contract.KEY_DATE, jsonObject.getString("date"));
            values.put(Contract.KEY_CHECK, jsonObject.getString("_check"));
        } catch (JSONException e){
            e.printStackTrace();
        }

        cr.insert(Contract.CONTENT_URI, values);
    }

    private String getToken(){
        final String AUDIENCE = "audience:server:client_id:";
        final String CLIENT_ID = "846170439833-fhs4tk4g5ce6hj17b25hkstcm1f3rjbv.apps.googleusercontent.com";
        final String SCOPE = AUDIENCE + CLIENT_ID;

        String[] emails = getUserEmails();
        String token = null;
        try {
            token = GoogleAuthUtil.getToken(getApplicationContext(), emails[0], SCOPE);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (GoogleAuthException e) {
            e.printStackTrace();
        }
        return token;
    }

    private String[] getUserEmails() {
        AccountManager manager = AccountManager.get(getApplicationContext());
        Account[] accounts = manager.getAccountsByType("com.google");
        String[] names = new String[accounts.length];
        for (int i = 0; i < accounts.length; i++) {
            names[i] = accounts[i].name;
            Log.d("NetworkClient", names[i]);
        }
        return names;
    }
}