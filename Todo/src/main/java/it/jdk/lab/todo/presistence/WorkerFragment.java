package it.jdk.lab.todo.presistence;

import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import it.jdk.lab.todo.Contract;

/**
 * Created by Flavio on 30/10/13.
 */
public class WorkerFragment extends Fragment {

    private CustomAsyncHandler handler;

   private class CustomAsyncHandler extends AsyncQueryHandler{

        public CustomAsyncHandler(ContentResolver cr) {
            super(cr);
        }
    }

    /**
     * Methods to manage the WorkerFragment
     *
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new CustomAsyncHandler(getActivity().getContentResolver());
        this.setRetainInstance(true);
        Log.d("TEST","I am "+this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
    }

    /**
     *  Methods to communicate with the ContentProvider.
     *
     */
    public void doQuery(Uri uri, String[] projection, String selection, String[] selectionArgs, String orderBy ){

        handler.startQuery(-1, null, uri, projection, selection, selectionArgs, orderBy);

    }

    public void doInsert(Uri uri, Bundle bundle){

        ContentValues values = new ContentValues();
        values.put(Contract.KEY_UUID, "");
        values.put(Contract.KEY_NAME, bundle.getString("name"));
        values.put(Contract.KEY_DATE, bundle.getString("date"));
        values.put(Contract.KEY_SYNC, 0);
        values.put(Contract.KEY_CHECK, 0);

        handler.startInsert(-1, null, uri, values);

    }

    public void doDelete(Uri uri, String selection, String[] selectionArgs){

        handler.startDelete(-1, null, uri, selection, selectionArgs);

    }

    public void doUpdate(Uri uri, ContentValues values, String selection, String[] selectionArgs){
        values.put(Contract.KEY_SYNC, Contract.NOT_SYNC);

        handler.startUpdate(-1, null, uri, values, selection, selectionArgs);

    }
}
