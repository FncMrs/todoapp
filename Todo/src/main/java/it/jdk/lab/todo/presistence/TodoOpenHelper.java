package it.jdk.lab.todo.presistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import it.jdk.lab.todo.Contract;

/**
 * Created by Jad1 on 25/10/13.
 */
public class TodoOpenHelper extends SQLiteOpenHelper {

    public TodoOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    // Called when no database exists in disk and the helper class needs to create a new one.
    @Override
    public void onCreate(SQLiteDatabase db) {
        // only when the db is created, we create the table
        db.execSQL(Constants.DATABASE_CREATE);
    }

    // Call during the update of DataBase, for example when we need to upgrade the version of itself
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Log the version upgrade.
        Log.w("TaskDBAdapter", "Upgrading from version " +
                oldVersion + " to " +
                newVersion + ", which will destroy all old data");

        db.execSQL("DROP TABLE IF EXISTS " + Contract.TABLE_NOTE_NAME);

        // Create tables again
        onCreate(db);

    }
}
