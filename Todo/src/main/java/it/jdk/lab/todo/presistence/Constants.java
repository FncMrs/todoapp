package it.jdk.lab.todo.presistence;

import android.content.UriMatcher;

import it.jdk.lab.todo.Contract;

class Constants {

    // Contract for Db info
    static final String DATABASE_NAME = "TodoDatabase";
    static final int DATABASE_VERSION = 1;

    //SQL table creation
    static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "
            + Contract.TABLE_NOTE_NAME + "("
            + Contract.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Contract.KEY_UUID + " TEXT, "
            + Contract.KEY_NAME + " TEXT, "
            + Contract.KEY_DATE + " TEXT, "
            + Contract.KEY_SYNC + " INTEGER, "
            + Contract.KEY_CHECK + " INTEGER )";

    static final int ALLROWS = 1;
    static final int SINGLE_ROW = 2;
    static final int SOME_ROWS = 3;
    static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    /**
     Populate the UriMatcher object, with URIs
     */
    static {

        URI_MATCHER.addURI(Contract.AUTHORITY, Contract.PATH, ALLROWS);
        URI_MATCHER.addURI(Contract.AUTHORITY, Contract.PATH + "/#", SINGLE_ROW);
        URI_MATCHER.addURI(Contract.AUTHORITY, Contract.PATH + "/*", SOME_ROWS);

    }
}