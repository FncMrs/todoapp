package it.jdk.lab.todo.list;

/**
 * Created by Jad1 on 16/10/13.
 */

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import it.jdk.lab.todo.Contract;
import it.jdk.lab.todo.R;
import it.jdk.lab.todo.util.CustomCheckBox;
import it.jdk.lab.todo.util.OnCheckEventListener;

public class CustomAdapter extends CursorAdapter implements OnCheckedChangeListener {

    private OnCheckEventListener myCheckListener;

    public CustomAdapter(Context context ) {
        super(context, null, FLAG_REGISTER_CONTENT_OBSERVER);
        myCheckListener = (OnCheckEventListener) context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {

            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.row, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.myName = (TextView)view.findViewById(R.id.lineItemName_id);
            viewHolder.myDate = (TextView)view.findViewById(R.id.lineItemDate_id);
            viewHolder.myCheck = (CustomCheckBox)view.findViewById(R.id.checkbox_id);
            viewHolder.mySync = (ImageView) view.findViewById(R.id.sync_id);

            view.setTag(viewHolder);
            return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        ViewHolder viewHolder = (ViewHolder) view.getTag();
        viewHolder.myName.setText(cursor.getString(cursor.getColumnIndex(Contract.KEY_NAME)));
        viewHolder.myDate.setText(cursor.getString(cursor.getColumnIndex(Contract.KEY_DATE)));
        viewHolder.myCheck.setTag(cursor.getLong(cursor.getColumnIndex(Contract.KEY_ID)));
        viewHolder.myCheck.setOnCheckedChangeListener(null);

        switch(cursor.getInt(cursor.getColumnIndex(Contract.KEY_SYNC))){
            case Contract.NOT_SYNC:
                viewHolder.mySync.setImageResource(R.drawable.not_sync_ic);
                break;
            case Contract.PENDING:
                viewHolder.mySync.setImageResource(R.drawable.pending_ic);
                break;
            case Contract.SYNC:
                viewHolder.mySync.setImageResource(R.drawable.sync_ic);
                break;
            default:
                break;
        }

        // Items check control
        if (cursor.getInt(cursor.getColumnIndex("_check")) == 1) {
            viewHolder.myCheck.setChecked(true);
        } else {
            viewHolder.myCheck.setChecked(false);
        }

        // Set onCheckedListener on checkbox
        viewHolder.myCheck.setOnCheckedChangeListener(CustomAdapter.this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            // Position of checked item
            Long id=(Long)buttonView.getTag();
            if(isChecked){
                myCheckListener.onCheckEvent(id, 1);
            }
            else{
                myCheckListener.onCheckEvent(id, 0);
            }

    }

    private class ViewHolder {
        public TextView myName;
        public TextView myDate;
        public CustomCheckBox myCheck;
        public ImageView mySync;
    }
}
