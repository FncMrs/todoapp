package it.jdk.lab.todo.list;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import it.jdk.lab.todo.Contract;
import it.jdk.lab.todo.R;
import it.jdk.lab.todo.actions.FormActivity;
import it.jdk.lab.todo.network.AuthenticationActivity;
import it.jdk.lab.todo.network.NetworkClient;
import it.jdk.lab.todo.presistence.WorkerFragment;
import it.jdk.lab.todo.services.TodoService;
import it.jdk.lab.todo.util.AboutUsActivity;
import it.jdk.lab.todo.util.OnActionListener;
import it.jdk.lab.todo.util.OnCheckEventListener;


public class ListActivity extends ActionBarActivity implements OnActionListener, OnCheckEventListener{

    private final static int REQUEST_CODE_INS = 1;
    private final static int REQUEST_CODE_MOD = 2;
    private final static int REQUEST_CODE_DEL = 3;
    private final static String WORKER_TAG = "worker";

    private WorkerFragment myWorkerFragment;
    private long myItemId; // Position of item selected

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if(savedInstanceState==null){
            myWorkerFragment = new WorkerFragment();
            fragmentTransaction.add(myWorkerFragment, WORKER_TAG);
            fragmentTransaction.commit();

            startService(new Intent(this, TodoService.class));

        } else {
            myWorkerFragment = (WorkerFragment)fragmentManager.findFragmentByTag(WORKER_TAG);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_insert:
                Intent insIntent = new Intent(this, FormActivity.class);
                insIntent.putExtra(FormActivity.TITLE_KEY, FormActivity.INS_TITLE);
                startActivityForResult(insIntent, REQUEST_CODE_INS);
                return true;

            case R.id.action_about:
                Intent aboutUsIntent = new Intent(this, AboutUsActivity.class);
                startActivity(aboutUsIntent);
                return true;

            case R.id.action_setting:
                Intent settingIntent = new Intent(this, AuthenticationActivity.class);
                startActivity(settingIntent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAction(int actionId, final Bundle bundle) {
        String idString = "";

        switch (actionId) {
            case ListFragment.DETAIL_ACTION:
                myItemId = bundle.getLong("id");
                Intent lineItemIntent = new Intent(ListActivity.this, LineItemActivity.class);
                lineItemIntent.putExtra("id", myItemId);
                startActivity(lineItemIntent);
                break;

            case ListFragment.DELETE_ACTION:
                idString = bundle.getString("ids");
                Log.d("MULTIPLE_DELETE",idString);
                NetworkClient.delete(this, "http://192.168.178.67:8080/todos", idString);
                //myWorkerFragment.doDelete(Uri.parse(Contract.CONTENT_URI+"/"+idString), null, null);
                break;

            case ListFragment.SHARE_ACTION:
                Long id = bundle.getLong("id");
                Log.d("SHARE", String.valueOf(id));

                NetworkClient.upload(this, "http://192.168.178.67:8080/todo", id);
                break;

            case ListFragment.SHARE_MORE_ACTION:
                idString = bundle.getString("ids");
                Log.d("SHARE", idString);               //1.9

                NetworkClient.upload(this, "http://192.168.178.67:8080/todos", idString);
                break;

            case ListFragment.START_MODE_ACTION:
                ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
                    @Override
                    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                        MenuInflater inflater = actionMode.getMenuInflater();
                        inflater.inflate(R.menu.action_mode_menu, menu);
                        return false;
                    }

                    @Override
                    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                        return false;
                    }

                    @Override
                    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                        return false;
                    }

                    @Override
                    public void onDestroyActionMode(ActionMode actionMode) {
                        actionMode = null;
                    }
                };

                startSupportActionMode(actionModeCallback);
                break;

            default:
                break;
        }
    }

    @Override
    public void onCheckEvent(Long id, int status) {

        String idString = String.valueOf(id);
        ContentValues values = new ContentValues();
        values.put(Contract.KEY_CHECK, status);
        myWorkerFragment.doUpdate(Uri.parse(Contract.CONTENT_URI + "/" + idString), values, null, null);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_CODE_INS:  // Insert new Item
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    myWorkerFragment.doInsert(Contract.CONTENT_URI, bundle);
                }
                break;

            case REQUEST_CODE_MOD:  // Modify a Item
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String idString = String.valueOf(myItemId);
                    ContentValues values = new ContentValues();
                    values.put(Contract.KEY_NAME, bundle.getString("name"));
                    values.put(Contract.KEY_DATE, bundle.getString("date"));
                    myWorkerFragment.doUpdate(Uri.parse(Contract.CONTENT_URI + "/" + idString), values, null, null);

                }
                break;

            case REQUEST_CODE_DEL:  // Delete a Item
                if (resultCode == RESULT_OK) {
                    String idString = String.valueOf(myItemId);
                    myWorkerFragment.doDelete(Uri.parse(Contract.CONTENT_URI+"/"+idString), null, null);
                }
                break;        }

    }
}