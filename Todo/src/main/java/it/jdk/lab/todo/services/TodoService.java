package it.jdk.lab.todo.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import it.jdk.lab.todo.R;
import it.jdk.lab.todo.list.ListActivity;

/**
 * Created by Flavio on 12/11/13.
 */
public class TodoService extends Service {

    private final int NOTIFICATION = R.string.todo_service_started;
    private final int REQUEST_CODE = 0;

    // This is the object that receives interactions from clients
    private IBinder myBinder;
    private NotificationManager myNotificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        myNotificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        showNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Cancel the persistent notification.
        myNotificationManager.cancel(NOTIFICATION);

        // Tell the user we stopped.
        Toast.makeText(this, R.string.todo_service_stopped, Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        if(myBinder==null)
            myBinder = new TodoBinder();
        return myBinder;
    }

    /**
     * Show a notification while this service is running.
     */
    private void showNotification() {

        CharSequence text = getText(R.string.todo_service_started);
        Log.d("LocalService", String.valueOf(text));

        NotificationCompat.Builder builderNotification = new NotificationCompat.Builder(this)
                                                .setContentTitle("2Do_2Day")
                                                .setContentText(text)
                                                .setSmallIcon(R.drawable.todo_icon);

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, REQUEST_CODE, new Intent(this, ListActivity.class),
                PendingIntent.FLAG_CANCEL_CURRENT);

        builderNotification.setContentIntent(contentIntent);
        myNotificationManager.notify(NOTIFICATION, builderNotification.build());

    }

    //Class for clients to access.
    public class TodoBinder extends Binder {
        TodoService getService() {
            return TodoService.this;
        }
    }
}
