package it.jdk.lab.todo.actions;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import it.jdk.lab.todo.Contract;
import it.jdk.lab.todo.R;
import it.jdk.lab.todo.util.OnActionListener;

/**
 * Created by Jad1 on 22/10/13.
 */
public class FormFragment extends Fragment implements OnClickListener,LoaderManager.LoaderCallbacks <Cursor> {

    public final static int FIELDS_EMPTY = 0;
    public final static int FIELDS_FILLED = 1;

    private EditText myFormName;
    private DatePicker myDatePicker;
    private Bundle myBundle;
    private OnActionListener myListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
             myListener = (OnActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Create, or inflate the Fragment’s UI, and return it.
        //If this Fragment has no UI then return null.
        myBundle = this.getActivity().getIntent().getExtras();
        View v = inflater.inflate(R.layout.form_fragment, container, false);
        TextView textViewTitle = (TextView) v.findViewById(R.id.formTitle_id);
        myFormName = (EditText) v.findViewById(R.id.formName_id);
        myDatePicker = (DatePicker) v.findViewById(R.id.formDatePicker_id);
        v.findViewById(R.id.formButton_id).setOnClickListener(this);

        if(myBundle.getInt(FormActivity.TITLE_KEY)==FormActivity.INS_TITLE)
            textViewTitle.setText("Inserisci nuova nota");
        else
            textViewTitle.setText("Modifica nota");

        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        if(myBundle.getInt(FormActivity.TITLE_KEY)==FormActivity.MOD_TITLE){
            final LoaderManager loaderManager = getActivity().getSupportLoaderManager();

            //Ensures a loader is initialized and active.
            loaderManager.initLoader( R.id.LOADER_ID,  // an identifier unique for the whole activity
                                      null,            // an optional bundle of args
                                      this);           //callbacks invoked during the life of the loader
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.formButton_id:
                Bundle bundle = new Bundle();
                if(myFormName.getText().toString()==null||myFormName.getText().toString().equals("")) {

                        myListener.onAction(FIELDS_EMPTY, bundle);
                }
                else{
                    bundle.putInt(FormActivity.TITLE_KEY, myBundle.getInt(FormActivity.TITLE_KEY));
                    bundle.putString("name", myFormName.getText().toString());
                    String time = String.valueOf(myDatePicker.getDayOfMonth())+"/"+
                            // The method getMonth returns a number from 0 to 11
                            String.valueOf(myDatePicker.getMonth()+1)+"/"+
                            String.valueOf(myDatePicker.getYear());

                    bundle.putString("date", time);

                    myListener.onAction(FIELDS_FILLED, bundle);
                }
                break;
        }
    }

    /**
     * Loader method
     *
     */
    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle bundle) {

        if(loaderId == R.id.LOADER_ID ){

            String selection="_id=?";
            String idString = String.valueOf(myBundle.getLong("id"));

            //Construct the new query in the form of a Cursor Loader.
            // Use the id parameter to contruct and return different loader.
            // Create the new Cursor Loader.
            CursorLoader loader = new CursorLoader( this.getActivity(),
                                                    Contract.CONTENT_URI,
                                                    Contract.PROJECTION,
                                                    selection,
                                                    new String[] {idString},
                                                    Contract.SORT_ORDER);

            return loader;
        } else {
                throw new AssertionError("Unhandled loader " + loaderId);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        final int id = loader.getId(); // which loader completed?

        if (id == R.id.LOADER_ID) {

            if(cursor!=null&&cursor.moveToFirst()){
                myFormName.setText((cursor.getString(cursor.getColumnIndexOrThrow(Contract.KEY_NAME))));

                String dateString = (cursor.getString(cursor.getColumnIndexOrThrow(Contract.KEY_DATE)));
                String[] date= dateString.split("/");
                int day=Integer.parseInt(date[0]);
                int month=Integer.parseInt(date[1]);
                int year=Integer.parseInt(date[2]);

                myDatePicker.updateDate(year, month-1, day);

            }
            // don't need to close the cursor.
        } else
            throw new AssertionError("Unhandled loader " + id);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
