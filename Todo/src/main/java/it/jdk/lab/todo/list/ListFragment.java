package it.jdk.lab.todo.list;

import android.app.Activity;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import it.jdk.lab.todo.Contract;
import it.jdk.lab.todo.R;
import it.jdk.lab.todo.util.OnActionListener;

/**
 * Created by Flavio on 27/10/13.
 */
public class ListFragment extends Fragment implements OnItemClickListener, OnItemLongClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    public final static int DETAIL_ACTION = 0;
    public final static int DELETE_ACTION = 1;
    public final static int SHARE_ACTION = 2;
    public final static int SHARE_MORE_ACTION = 3;
    public final static int START_MODE_ACTION = 4;
    public CustomAdapter myAdapter;
    //public CustomAdapterActionMode myAdapterActionMode;
    public ListView myListView;
    public OnActionListener myActionListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            myActionListener = (OnActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.list_fragment, container, false);
        myListView = (ListView) v.findViewById(R.id.listView_id);

        // ListView filling
        myAdapter = new CustomAdapter(this.getActivity());
        //myAdapterActionMode = new CustomAdapterActionMode(this.getActivity());

        myListView.setOnItemClickListener(this);
        myListView.setOnItemLongClickListener(this);
        myListView.setAdapter(myAdapter);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        final LoaderManager loaderManager = getActivity().getSupportLoaderManager();

        //Ensures a loader is initialized and active.
        loaderManager.initLoader(R.id.LOADER_ID,  // an identifier unique for the whole activity
                                  null,            // an optional bundle of args
                                  this);           //callbacks invoked during the life of the loader

    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Bundle bundle = new Bundle();
        bundle.putLong("id", id);
        myActionListener.onAction(DETAIL_ACTION, bundle);

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, final long id) {
        myListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

            myListView.setMultiChoiceModeListener(new MultiChoiceModeListener() {

                private List<Long> selectedRows = new ArrayList<Long>();
                int count = 0;

                @Override
                public void onItemCheckedStateChanged(android.view.ActionMode mode, int position, long id, boolean checked) {

                    if (checked) {
                        selectedRows.add(id);
                        count++;
                    } else {
                        selectedRows.remove(id);
                        count--;
                    }
                    //mode.setTitle(selectedRows.toString());
                    mode.setTitle("Elementi Selezionati: " + count);
                }

                @Override
                public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.action_mode_menu, menu);
                    View v = mode.getCustomView();
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                    // Respond to clicks on the actions in the CAB
                    Bundle bundle = new Bundle();
                    String selectedItems = "";

                    switch (item.getItemId()) {
                        case R.id.actionModeDelete_id:
                            for (Long l : selectedRows) {
                                if (l.equals(selectedRows.get(0)))
                                    selectedItems = l.toString();
                                else
                                    selectedItems += ", " + l;
                            }
                            bundle.putString("ids", selectedItems);

                            myActionListener.onAction(DELETE_ACTION, bundle);
                            mode.finish(); // Action picked, so close the CAB
                            return true;

                        case R.id.actionModeShare_id:
                            if (count == 1) {
                                bundle.putLong("id", selectedRows.get(0));
                                myActionListener.onAction(SHARE_ACTION, bundle);
                            } else if (count > 1) {
                                for (Long l : selectedRows) {
                                    if (l.equals(selectedRows.get(0)))
                                        selectedItems = l.toString();
                                    else
                                        selectedItems += ", " + l;
                                }
                                bundle.putString("ids", selectedItems);
                                myActionListener.onAction(SHARE_MORE_ACTION, bundle);
                            }

                            mode.finish(); // Action picked, so close the CAB
                            return true;

                        default:
                            return false;
                    }
                }

                @Override
                public void onDestroyActionMode(android.view.ActionMode mode) {
                    selectedRows.clear();
                    count = 0;
                    mode = null;
                }
            });
        }
        else
            myActionListener.onAction(START_MODE_ACTION, null);

        return true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle bundle) {

        if (loaderId == R.id.LOADER_ID) {
            //Construct the new query in the form of a Cursor Loader.
            // Use the id parameter to contruct and return different loader.

            // Create the new Cursor Loader.
            CursorLoader loader = new CursorLoader( this.getActivity(),
                                                    Contract.CONTENT_URI,
                                                    Contract.PROJECTION,
                                                    null,
                                                    null,
                                                    Contract.SORT_ORDER);

            return loader;
        }
        else
            throw new AssertionError("Unhandled loader " + loaderId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

        final int id = loader.getId(); // which loader completed?

        if (id == R.id.LOADER_ID) {

            // Replace the result Cursor displayed by the Cursor Adapter with
            // the new result set.
            myAdapter.swapCursor(cursor);
            //myListView.setAdapter(myAdapter);

            // don't need to close the cursor.
        } else
            throw new AssertionError("Unhandled loader " + id);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

        final int id = loader.getId();
        if (id == R.id.LOADER_ID) {
            // Remove the existing result Cursor from the List Adapter.
            myAdapter.swapCursor(null);

        } else
            throw new AssertionError("Unhandled loader " + id);
    }

}
