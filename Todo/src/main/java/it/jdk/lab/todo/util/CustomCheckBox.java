package it.jdk.lab.todo.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

/**
 * Created by Flavio on 24/10/13.
 */
public class CustomCheckBox extends CheckBox {

    private OnCheckedChangeListener mListener;

    public CustomCheckBox(Context context) {
        super(context);
    }

    public CustomCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        super.setOnCheckedChangeListener(listener);
        mListener = listener;
    }

    public void setCheckedSilently(boolean checked) {
        OnCheckedChangeListener  l = mListener;
        setOnCheckedChangeListener(null);
        super.setChecked(checked);
        setOnCheckedChangeListener(l);
    }
}
