package it.jdk.lab.todo.util;

import android.app.Activity;
import android.os.Bundle;

import it.jdk.lab.todo.R;

/**
 * Created by Flavio on 06/11/13.
 */
public class AboutUsActivity extends Activity{

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us_activity);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
