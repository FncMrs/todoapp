package it.jdk.lab.todo.util;

import android.os.Bundle;

/**
 * Created by Frankie on 24/10/13.
 *
 * Interface for callback from Fragment to Activity
 *
 */

public interface OnActionListener {

    public void onAction(int actionId, Bundle bundle);

}
