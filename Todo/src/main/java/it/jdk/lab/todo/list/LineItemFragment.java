package it.jdk.lab.todo.list;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.jdk.lab.todo.Contract;
import it.jdk.lab.todo.R;

/**
 * Created by Jad1 on 22/10/13.
 */
public class LineItemFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private TextView myLineName;
    private TextView myLineDate;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Create, or inflate the Fragment’s UI, and return it.
        //If this Fragment has no UI then return null.
        View v =inflater.inflate(R.layout.line_item_fragment, container, false);

        myLineName = (TextView)v.findViewById(R.id.lineitName_id);
        myLineDate = (TextView)v.findViewById(R.id.lineitDate_id);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(R.id.LOADER_ID, null, this);

    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle bundle) {

        if (loaderId == R.id.LOADER_ID) {

            //Construct the new query in the form of a Cursor Loader.
            // Use the id parameter to contruct and return different loader.

            String idString = String.valueOf(getActivity().getIntent().getLongExtra("id", 0));

            // Create the new Cursor Loader.
            CursorLoader loader = new CursorLoader( getActivity(),
                                                    Uri.parse(Contract.CONTENT_URI + "/" + idString),
                                                    Contract.PROJECTION,
                                                    null,
                                                    null,
                                                    Contract.SORT_ORDER);

            return loader;
        }
        else
            throw new AssertionError("Unhandled loader " + loaderId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {

        // Textviews update
        final int id = cursorLoader.getId();
        if (id == R.id.LOADER_ID) {
            if(cursor.moveToFirst()){
                myLineName.setText("Testo: "+ cursor.getString(cursor.getColumnIndexOrThrow(Contract.KEY_NAME)));
                myLineDate.setText("Data: "+ cursor.getString(cursor.getColumnIndexOrThrow(Contract.KEY_DATE)));
            }
        }
        else
            throw new AssertionError("Unhandled loader " + id);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

}