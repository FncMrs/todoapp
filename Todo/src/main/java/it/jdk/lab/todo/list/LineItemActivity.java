package it.jdk.lab.todo.list;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import it.jdk.lab.todo.BaseActivity;
import it.jdk.lab.todo.Contract;
import it.jdk.lab.todo.R;
import it.jdk.lab.todo.actions.DeleteActivity;
import it.jdk.lab.todo.actions.FormActivity;
import it.jdk.lab.todo.presistence.WorkerFragment;

public class LineItemActivity extends ActionBarActivity {

    private final static int REQUEST_CODE_MOD = 1;
    private final static int REQUEST_CODE_DEL = 2;
    private final static String WORKER_TAG = "workerFrag";
    private WorkerFragment myWorkerFragment;
    private Long myId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.line_item_activity);

        myId = this.getIntent().getLongExtra("id", 0);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(savedInstanceState==null){
            myWorkerFragment = new WorkerFragment();
            fragmentTransaction.add(myWorkerFragment, WORKER_TAG);
            fragmentTransaction.commit();
        } else {
            myWorkerFragment = (WorkerFragment)fragmentManager.findFragmentByTag(WORKER_TAG);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.line_item_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionModify_id:
                Intent modIntent = new Intent(LineItemActivity.this, FormActivity.class);
                modIntent.putExtra(FormActivity.TITLE_KEY, FormActivity.MOD_TITLE);
                modIntent.putExtra("id", myId);
                startActivityForResult(modIntent, REQUEST_CODE_MOD);
                return true;

            case R.id.actionDelete_id:
                Intent del_intent = new Intent(this, DeleteActivity.class);
                startActivityForResult(del_intent, REQUEST_CODE_DEL);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==REQUEST_CODE_MOD && resultCode==RESULT_OK) {
            // Update item details
            Bundle bundleFromFragment = data.getExtras();
            String  idString = String.valueOf(myId);

            ContentValues values = new ContentValues();
            values.put(Contract.KEY_NAME, bundleFromFragment.getString("name"));
            values.put(Contract.KEY_DATE, bundleFromFragment.getString("date"));
            myWorkerFragment.doUpdate(Uri.parse(Contract.CONTENT_URI+"/"+idString), values , null, null);
        }
        else if (requestCode==REQUEST_CODE_DEL && resultCode==RESULT_OK){
            // Delete item
            String  idString = String.valueOf(myId);
            myWorkerFragment.doDelete(Uri.parse(Contract.CONTENT_URI+"/"+idString), null, null);
            finish();
        }
    }

}
