package it.jdk.lab.todo;

import android.net.Uri;

/**
 * Created by Jad1 on 25/10/13.
 */
public class Contract {

    // SQL columns declaration
    public static final String KEY_ID = "_id";
    public static final String KEY_UUID = "uuid";
    public static final String KEY_NAME = "name";
    public static final String KEY_DATE = "date";
    public static final String KEY_SYNC = "sync";
    public static final String KEY_CHECK = "_check";
    public static final String TABLE_NOTE_NAME = "Note";

    // Define a projection that specifies which columns from the database
    // you will actually use after this query.(columns to return)
    public static final String[] PROJECTION = { KEY_ID, KEY_UUID, KEY_NAME,KEY_DATE, KEY_SYNC, KEY_CHECK };

    // How you want the results sorted in the resulting Cursor
    public static final String SORT_ORDER = null; //KEY_DATE + " DESC ";

    // Replace these with valid SQL statements if necessary.
    public static final String GROUP_BY = null;
    public static final String HAVING = null;

    // Addresses
    public static final String AUTHORITY = "it.jdk.lab.presistence.todoprovider";
    public static final String PATH = "myNotes";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + PATH);

    // Sync statuses
    public static final int NOT_SYNC = 0;
    public static final int PENDING = 1;
    public static final int SYNC = 2;

}
