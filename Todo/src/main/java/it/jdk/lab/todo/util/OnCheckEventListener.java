package it.jdk.lab.todo.util;

/**
 * Created by Flavio on 03/11/13.
 */
public interface OnCheckEventListener {

    public void onCheckEvent(Long id, int status);

}
